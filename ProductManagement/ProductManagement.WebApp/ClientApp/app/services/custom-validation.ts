﻿import { AbstractControl, ValidatorFn } from "@angular/forms";

export class CustomValidationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            "required": `${validatorValue} is required.`
        };

        return config[validatorName];
    }

    static required(fieldName: string) {
        return (control: AbstractControl) => {
            if (!control.value) {
                return { "required": fieldName };
            }

            return null;
        }
    }
}