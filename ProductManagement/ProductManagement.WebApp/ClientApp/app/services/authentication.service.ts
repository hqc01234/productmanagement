﻿import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { UserManager, UserManagerSettings, User, WebStorageStateStore } from "oidc-client";
import { isBrowser } from "./utilities.service";
import { UserRole } from "../common/enums/common.enum";

export function getOidcClientSettings(): UserManagerSettings {
    return {
        client_id: "angular_client",
        authority: "http://localhost:5001",
        redirect_uri: "http://localhost:5000/#/authentication-callback#",
        post_logout_redirect_uri: "http://localhost:5000",
        silent_redirect_uri: "http://localhost:5000/signin-silent-callback.html",
        response_type: "id_token token",
        scope: "openid profile product_management_api role",
        filterProtocolClaims: true,
        loadUserInfo: true,
        userStore: new WebStorageStateStore({ store: window.localStorage })
    }
};

@Injectable()
export class AuthenticationService {
    public userManager: UserManager;
    public currentUser: User;
    public userLoadededEvent: EventEmitter<User> = new EventEmitter<User>();

    constructor() {
        if (isBrowser()) { //dont run this if render from server
            this.userManager = new UserManager(getOidcClientSettings());

            //clears any old requests from storage
            this.userManager.clearStaleState();

            //set current user
            this.userManager.getUser().then((user) => {
                if (user) {
                    if (!user.expired) {
                        this.currentUser = user;
                        this.userLoadededEvent.emit(user);
                    } else {
                        this.userManager.removeUser();
                    }
                }
            });

            //event when token expired
            this.userManager.events.addAccessTokenExpired(() => {
                this.userManager.signinSilent().then((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.userLoadededEvent.emit(user);
                    }
                });
            });
        }
    }

    public isLoggedInPromise(): Promise<boolean> {
        return this.userManager.getUser().then((user) => {
            return user && !user.expired;
        });
    }
    
    public isLoggedIn(): boolean {
        return this.currentUser != null && !this.currentUser.expired;
    }

    public isInRole(roleName: UserRole): boolean {
        return this.isLoggedIn() && this.currentUser.profile.role == roleName;
    }

    public getClaims(): any {
        return this.currentUser.profile;
    }

    public startAuthentication(): Promise<void> {
        return this.userManager.signinRedirect();
    }

    public signOutRedirect(): Promise<void> {
        return this.userManager.signoutRedirect();
    }

    public authenticationCallBack(url: string): Promise<User> {
        return this.userManager.signinRedirectCallback(url).then((user) => {
            if (user) {
                this.currentUser = user;
                this.userLoadededEvent.emit(user);
            }
            return user;
        });
    }

    public getAuthorizationHeaderValue(): string {
        return `${this.currentUser.token_type} ${this.currentUser.access_token}`;
    }
}
