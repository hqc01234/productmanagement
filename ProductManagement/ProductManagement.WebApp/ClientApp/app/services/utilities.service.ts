﻿import { Injectable, NgZone } from "@angular/core";

/** Check if code run from browser */
export function isBrowser() {
    return typeof Window != 'undefined';
}

if (isBrowser()) {
    require("admin-lte/dist/js/app.js");
}

declare const $: AdminLTE_JQueryObject;

@Injectable()
export class UtilitiesService {

    constructor(private zone: NgZone) {
    }

    /** Customize AdminLTE tree event */
    public overrideTreeEvent(menu: string) {
        var animationSpeed = $.AdminLTE.options.animationSpeed;
        $(document).ready(() => {
            $(document).off("click", menu + " li a").on("click", menu + " li a", function (e) {
                //Get the clicked link and the next element
                var $this = $(this);
                var checkElement = $this.next();

                //Check if the next element is a menu and is visible
                if ((checkElement.is(".treeview-menu")) && (checkElement.is(":visible")) && (!$("body").hasClass("sidebar-collapse"))) {
                    //Close the menu
                    checkElement.slideUp(animationSpeed, function () {
                        checkElement.removeClass("menu-open");
                    });
                    if (checkElement.find("li.active")[0] == undefined) {
                        checkElement.parent("li").removeClass("active");
                    }
                    checkElement.parent("li").find(".fa.fa-angle-left.pull-right").css("transform", "inherit");
                }
                //If the menu is not visible
                else if ((checkElement.is(".treeview-menu")) && (!checkElement.is(":visible"))) {
                    //Get the parent menu
                    var parent = $this.parents("ul").first();
                    //Close all open menus within the parent
                    var ul = parent.find("ul:visible").slideUp(animationSpeed);
                    ul.prev().find(".fa.fa-angle-left.pull-right").css("transform", "inherit");
                    //Remove the menu-open class from the parent
                    ul.removeClass("menu-open");
                    //Get the parent li
                    var parent_li = $this.parent("li");

                    //Open the target menu and add the menu-open class
                    checkElement.slideDown(animationSpeed, function () {
                        //Add the class active to the parent li
                        checkElement.addClass("menu-open");
                        checkElement.parent("li").find(".fa.fa-angle-left.pull-right").css("transform", "");
                        parent_li.addClass("active");
                        //Fix the layout in case the sidebar stretches over the height of the window
                        $.AdminLTE.layout.fix();
                    });
                }
                //if this isn"t a link, prevent the page from being redirected
                if (checkElement.is(".treeview-menu")) {
                    e.preventDefault();
                }
            });
        });
    }

    /** Fix the height of the window */
    public fixLayout(delayTime: number) {
        this.zone.runOutsideAngular(() => {
            $(document).ready(() => {
                setTimeout(() => {
                    $.AdminLTE.layout.fix();
                }, delayTime);
            });
        });
    }
}