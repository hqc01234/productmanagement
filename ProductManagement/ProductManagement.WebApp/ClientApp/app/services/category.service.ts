﻿import { Injectable } from "@angular/core";
import { HttpHelpers } from "../services/all.services";

@Injectable()
export class CategoryService {

    private save_category_url = "api/Category/SaveCategory";
    private delete_category_url = "api/Category/DeleteCategory";
    private edit_category_url = "api/Category/EditCategory";

    constructor(private httpHelper: HttpHelpers) {

    }

    public saveCategory(category: CategoryTableModel) {
        return this.httpHelper.post({
            url: this.save_category_url,
            data: category
        });
    }

    public deleteCategory(category: CategoryTableModel) {
        return this.httpHelper.post({
            url: this.delete_category_url,
            data: category
        });
    }

    public editCategory(category: CategoryTableModel) {
        return this.httpHelper.post({
            url: this.edit_category_url,
            data: category
        });
    }
}