﻿import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthenticationService } from "./authentication.service";
import { isBrowser } from "./utilities.service";
import { UserRole } from "../common/enums/common.enum";

@Injectable()
export class AuthenticationGuard implements CanActivate {

    constructor(
        private authService: AuthenticationService,
        private router: Router) {
    }

    public canActivate() {
        if (isBrowser()) { //dont run this if render from server
            return this.authService.isLoggedInPromise().then((isLoggedin) => {
                if (!isLoggedin) {
                    this.authService.startAuthentication();
                } else if (!this.authService.isInRole(UserRole.Admin)) {
                    this.router.navigate(["unauthorized"]);
                };
                return isLoggedin;
            })
        }
        else {
            return false;
        }
    }
}
