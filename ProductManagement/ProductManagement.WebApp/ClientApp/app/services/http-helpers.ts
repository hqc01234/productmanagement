﻿import { Injectable, Inject } from "@angular/core";
import { Location } from "@angular/common";
import { Http, URLSearchParams, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from "../services/all.services";
import "rxjs/add/operator/map";

@Injectable()
export class HttpHelpers {

    private headers: Headers = new Headers({ "Content-Type": "application/json" });

    constructor(
        @Inject("BASE_URL")
        private baseUrl: string,
        private http: Http,
        private location: Location,
        private authService: AuthenticationService) {

        this.setHeaders();
    }

    public get<T>(option: HttpParameters): Observable<T> {
        let url = this.resolveUrl(option);
        let params = this.prepareParams(option);

        return this.http.get(url, { params: params, headers: this.headers })
            .map((response) => {
                return <T>response.json();
            });
    }

    public post(option: HttpParameters): Observable<any> {
        let url = this.resolveUrl(option);
        let params = this.prepareParams(option);

        return this.http.post(url, null, { params: params, headers: this.headers })
            .map((response) => {
                return response.json();
            });
    }

    /** Resolve url or return base url if option = null */
    public resolveUrl(option?: HttpParameters) {
        if (option && option.url) {
            var nomalizeUrl = this.location.normalize(option.url);
            if (nomalizeUrl.indexOf("/") == 0) {
                nomalizeUrl = nomalizeUrl.replace("/", "");
            }
            return `${this.baseUrl}${nomalizeUrl}`;
        }
        return `${this.baseUrl}`;
    }

    private setHeaders() {
        this.headers.set("Authorization", this.authService.getAuthorizationHeaderValue());
    }

    private prepareParams(option: HttpParameters) {
        let params = new URLSearchParams();

        if (option && option.data) {
            for (let key in option.data) {
                params.set(key, typeof option.data[key] == "object" ? JSON.stringify(option.data[key]) : option.data[key]);
            }
        }

        return params;
    }
}