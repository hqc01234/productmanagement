import { Component, HostListener } from "@angular/core";
import { UtilitiesService, AuthenticationService, isBrowser } from "../../services/all.services";
import { UserRole } from "../../common/enums/common.enum";
import { User } from "oidc-client";

@Component({
    selector: "app",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.css"]
})
export class AppComponent {
    public isLoggedIn: boolean = false;
    public isHavePermission: boolean = false;

    constructor(
        private authService: AuthenticationService,
        private utils: UtilitiesService) {

        this.authService.userLoadededEvent.subscribe((user: User) => {
            this.isLoggedIn = this.authService.isLoggedIn();
            this.isHavePermission = this.authService.isInRole(UserRole.Admin);
            this.utils.overrideTreeEvent(".sidebar");
            this.utils.fixLayout(0);
        })
    }

    @HostListener("window:resize", ["$event"])
    private onWindowResize(event) {
        this.utils.fixLayout(300);
    }

    public signOut() {
        this.authService.signOutRedirect();
    }
}
