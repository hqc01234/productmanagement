﻿import { Component } from "@angular/core";
import { NavigationItem } from "../../models/all.models";
import { FontAwesomeIcon } from "../../common/enums/common.enum";

@Component({
    selector: "admin-navigation",
    templateUrl: "admin.navigation.component.html"
})
export class AdminNavigationComponent {

    public navigationItems: Array<NavigationItem>;

    constructor() {
        this.navigationItems = [
            new NavigationItem({
                name: "Product Management",
                icon: FontAwesomeIcon.fa_image,
                openByDefault: true,
                childItems: [
                    {
                        name: "Category",
                        routeLink: "/category",
                        icon: FontAwesomeIcon.fa_circle_o
                    },
                    {
                        name: "Product",
                        routeLink: "/product",
                        icon: FontAwesomeIcon.fa_circle_o
                    },
                ]
            })
        ];
    }
}