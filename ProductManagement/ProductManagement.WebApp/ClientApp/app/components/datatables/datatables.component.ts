﻿import {
    Component, Input, AfterViewInit, NgZone, ComponentFactoryResolver, ApplicationRef,
    ElementRef, ViewEncapsulation, EventEmitter, ViewContainerRef, Injector, ComponentRef
} from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";

import { DataTablesSetting, DataTablesColumn, SelectEventData } from "../../models/all.models";
import { DefaultDataTablesLanguageSetting } from "../../common/constants/language.constant";
import { UtilitiesService, AuthenticationService, isBrowser } from "../../services/all.services";
import { ANIMATION_TYPES } from "ngx-loading";
import { ILoadingConfig } from "../../../../node_modules/ngx-loading/src/ngx-loading.config";
import { sortBy } from "lodash";

if (isBrowser()) { //jquery dont work when render from server
    require("datatables.net/js/jquery.dataTables.js");
    require("datatables.net-bs/js/dataTables.bootstrap.js");
    require("datatables.net-responsive/js/dataTables.responsive.min.js");
    require("datatables.net-responsive-bs/js/responsive.bootstrap.min.js");
    require("datatables.net-select/js/dataTables.select.min.js");
    require("jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js");
}

@Component({
    selector: "datatables",
    templateUrl: "datatables.component.html",
    styleUrls: ["datatables.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class DatatableComponent implements AfterViewInit {

    @Input("dataTablesSetting") inputDtSetting: DataTablesSetting<any>;

    public dtApi: DataTables.Api;
    public dtJqueryObject: JQuery<HTMLElement>;
    public loading: boolean = false;
    public ngxLoadingConfig: ILoadingConfig = {
        animationType: ANIMATION_TYPES.rectangleBounce,
        backdropBackgroundColour: "rgba(0, 0, 0, 0.075)"
    };

    /** Called every time DataTables performs a draw */
    public onDrawCallBack: EventEmitter<DataTables.Settings> = new EventEmitter();

    /** Triggered whenever an item (rows, columns or cells) are selected*/
    public onSelect: EventEmitter<SelectEventData> = new EventEmitter();

    /** Triggered whenever an item (rows, columns or cells) are deselected*/
    public onDeselect: EventEmitter<SelectEventData> = new EventEmitter();
  
    private showInfoSelected: Function;
    private setting: DataTables.Settings;

    constructor(
        private zone: NgZone,
        private elementRef: ElementRef,
        private utils: UtilitiesService,
        private authService: AuthenticationService,
        private componentFactoryResolver: ComponentFactoryResolver,
        private applicationRef: ApplicationRef,
        private injector: Injector) {
    }

    public ngAfterViewInit() {
        if (isBrowser()) {
            this.dtJqueryObject = $(`#${this.inputDtSetting.tableId}`);
            this.showInfoSelected = $.fn.DataTable["Checkboxes"].prototype.showInfoSelected;
            this.initDataTables();
        }
    }

    public getSelectedData(): Array<any> {
        return this.dtApi.rows({ selected: true }).data().toArray();
    }

    public getAllData(): Array<any> {
        return this.dtApi.rows().data().toArray();
    }

    private initDataTables() {
        //some default setting
        this.setting = {};
        this.setting.dom = this.inputDtSetting.dom || "<'pull-right'l>tip";
        this.setting.serverSide = this.inputDtSetting.serverSide != undefined ?
            this.inputDtSetting.serverSide : true;
        this.setting.processing = this.inputDtSetting.processing != undefined ?
            this.inputDtSetting.processing : true;     
        this.setting.language = Object.assign({}, DefaultDataTablesLanguageSetting, this.inputDtSetting.language);

        //fix issue that dt call ajax twice when init
        this.setting.deferLoading = 0;
        this.setting.lengthMenu = [[5, 10, 15, 20], [5, 10, 15, 20]]

        this.setupAjaxSource();
        this.setupColumnsSetting();
        this.setupSelectPlugin();
        this.setupResponsivePlugin();
        this.setupProcessingEvent();
        this.setupInitEvent();

        //init table
        this.dtApi = this.dtJqueryObject.DataTable(this.setting);

        //setup some event after init
        this.setupEventAfterInit();
    }

    private setupAjaxSource() {
        if (this.inputDtSetting.ajax) {
            this.setting.ajax = {};
            this.setting.ajax.url = this.inputDtSetting.ajax.url || ""
            this.setting.ajax.type = this.inputDtSetting.ajax.type || "GET";
            this.setting.ajax.headers = {
                Authorization: this.authService.getAuthorizationHeaderValue()
            }

            if (this.inputDtSetting.ajax.data) {
                this.setting.ajax.data = this.inputDtSetting.ajax.data;
            }
        } else if (this.inputDtSetting.data) {
            this.setting.data = this.inputDtSetting.data; //data from object
            this.setting.serverSide = false; //no need server side
        }
    }

    private setupColumnsSetting() {
        //setup default sort 
        this.inputDtSetting.columns = this.inputDtSetting.columns.map((value, index) => {
            value["index"] = index; //set index for column
            return value;
        });

        var defaultSortColumns = this.inputDtSetting.columns.filter(x => { return x.defaultSort != null; });
        var order = [];
        if (defaultSortColumns.length > 0) {
            //order by sortOrder, lower = sort first
            defaultSortColumns = sortBy(defaultSortColumns, function (column) { return [column.defaultSort.sortOrder, column["index"]]; });
            defaultSortColumns.forEach((value) => {
                order.push([value["index"], value.defaultSort.sortType])
            });
        }
        this.setting.order = order;

        //setup column
        this.setting.columns = [];
        this.inputDtSetting.columns.forEach(column => {
            let columnSetting: DataTables.ColumnSettings = {
                title: column.title,
                data: column.data,
                orderable: column.sortable != null ? column.sortable : true,
                visible: column.visible != null ? column.visible : true,
                width: column.width != null ? column.width : "",
                className: column.className != null ? column.className : ""
            };
            if (column.render) {
                columnSetting.render = column.render;
            }
            this.setting.columns.push(columnSetting);
        });
    }

    private setupSelectPlugin() {
        //default select plugin
        let defaultSelectOption = { enable: true, style: "single", info: false, checkBox: true };
        this.inputDtSetting.select = Object.assign({}, defaultSelectOption, this.inputDtSetting.select);

        //setup select
        if (this.inputDtSetting.select.enable) {
            this.setting["select"] = {
                style: this.inputDtSetting.select.style,
                info: this.inputDtSetting.select.info,
                //selector: "td.dt-checkboxes-cell"
            };

            //setup selected checkbox
            //more info: https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
            if (this.inputDtSetting.select.checkBox) {
                let columnSetting: DataTables.ColumnSettings = {
                    title: "Select",
                    data: "Id", //Column containing checkboxes must have unique data => use Id
                    orderable: false,
                    searchable: false,
                    className: "dt-checkboxes-select-all",
                    render: function (data, type, row, meta) {
                        if (type === "display") {
                            data = `<div class="abc-checkbox abc-checkbox-success">
                                        <input type="checkbox" class="dt-checkboxes">
                                        <label></label>
                                    </div>`;
                        }
                        return data;
                    }
                };
                columnSetting["checkboxes"] = {
                    selectRow: true,
                    selectAll: false,
                    selectAllPages: false
                };
                columnSetting["responsivePriority"] = 0; //work arround for issue that select box dont work when hide by dt responsive plugin
                this.setting.columns[0]["responsivePriority"] = 1; //TODO: add settup for responsivePriority
                this.setting.columns.push(columnSetting);
            }
        }
    }

    private setupResponsivePlugin() {
        this.setting["responsive"] = {
            details: {
                type: "column",
                target: this.setting.columns.length
            }
        };

        this.setting.columns.push({ //add column for responsive control
            data: null,
            className: "control dt-responsive-control",
            defaultContent: "",
            orderable: false,
            searchable: false
        });
    }

    private setupInitEvent() {
        this.dtJqueryObject.on("init.dt", () => {
            //setup show/hide select info if using checkbox plugin
            if (this.inputDtSetting.select.enable && this.inputDtSetting.select.checkBox && !this.inputDtSetting.select.info) {
                $.fn.DataTable["Checkboxes"].prototype.showInfoSelected = () => { };
            } else {
                $.fn.DataTable["Checkboxes"].prototype.showInfoSelected = this.showInfoSelected;
            }

            this.zone.runOutsideAngular(() => {
                setTimeout(() => {
                    this.dtApi.columns.adjust()["responsive"].recalc();
                    this.removeTabIndex();
                }, 0);
            });
        });
    }

    private setupProcessingEvent() {
        if (this.setting.processing) {
            this.dtJqueryObject.on("processing.dt", (e, settings, processing) => {
                if (processing) {
                    //work arround for ExpressionChangedAfterItHasBeenCheckedError
                    setTimeout(() => { this.loading = processing; }, 0);
                } else {
                    setTimeout(() => { this.loading = processing; }, 600);
                }
            });
        }
    }

    private setupEventAfterInit() {
        this.dtApi.on("responsive-resize", (e, datatable: DataTables.Api, columns) => {
            //fix issue with column width when resize screen
            this.zone.runOutsideAngular(() => {
                setTimeout(() => {
                    datatable.columns.adjust()["responsive"].recalc();
                }, 200);
            });
        });

        this.dtApi.on("draw.dt", (e, settings: DataTables.Settings) => {
            this.removeTabIndex();
            this.onDrawCallBack.emit(settings);
        });
        
        this.dtApi
            .on("select", (e, dt, type, index) => {
                let data = this.dtApi.row(index).data();
                this.onSelect.emit({ rowIndex: index[0], data: data });
            }).on("deselect", (e, dt, type, index) => {
                let data = this.dtApi.row(index).data();
                this.onDeselect.emit({ rowIndex: index[0], data: data });
            }).on("user-select", (e, dt, type, indexes) => {
                //TODO
            });
    }

    private removeTabIndex() {
        var allElement = (<HTMLElement>this.elementRef.nativeElement).querySelectorAll("[tabindex]");
        for (let i = 0; i < allElement.length; i++) {
            allElement[i].removeAttribute("tabindex");
        }
    }
}