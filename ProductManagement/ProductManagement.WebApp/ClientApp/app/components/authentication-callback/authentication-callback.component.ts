import { Component } from "@angular/core";
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { UserRole } from "../../common/enums/common.enum";
import { AuthenticationService, isBrowser } from "../../services/all.services";
import * as Oidc from "oidc-client";

@Component({
    selector: "authentication-callback",
    templateUrl: "authentication-callback.component.html"
})
export class AuthenticationCallBackComponent {

    constructor(
        private authService: AuthenticationService,
        private router: Router,
        private location: Location) {

        if (isBrowser()) {
            var query = window.location.href.split("#").slice(2).join("#");
            this.authService.authenticationCallBack(query).then((user) => {
                if (user) {
                    if (user.profile.role == UserRole.Admin) {
                        this.router.navigate(["category"]);
                    } else {
                        this.router.navigate(["unauthorized"]);
                    }
                }
            }).catch((error) => {
                this.router.navigate(["internal-server-error"]);
            });
        }
    }
}
