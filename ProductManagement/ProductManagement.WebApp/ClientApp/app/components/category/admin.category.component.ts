﻿import { Component, ViewChild, AfterViewInit, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { ColumnSortType, CategoryComponentEvent } from "../../common/enums/common.enum";
import { HttpHelpers, CategoryService } from "../../services/all.services";
import { DataTablesSetting, SelectEventData } from "../../models/all.models";
import { DatatableComponent } from "../../components/datatables/datatables.component";

@Component({
    selector: "admin-category",
    templateUrl: "admin.category.component.html",
    styleUrls: ["admin.category.component.css"]
})
export class AdminCategoryComponent implements AfterViewInit {

    @ViewChild("categoryTable")
    public categoryTable: DatatableComponent;
    public categoryTableSetting: DataTablesSetting<CategoryTableModel>;
    public isShowCategoryDetail: boolean = false;
    public isDisabledDeleteBtn: boolean = true;

    public actionEvent: EventEmitter<ActionEventModel> = new EventEmitter();
    
    constructor(
        private httpHelper: HttpHelpers,
        private formBuilder: FormBuilder,
        private categoryService: CategoryService) {

        this.setupCategoryTable();
        this.subscribeActionEvent();
    }

    public ngAfterViewInit() {
        this.categoryTable.onSelect.subscribe((data: SelectEventData) => {
            this.onTableRowClick(data);
        });

        this.categoryTable.onDeselect.subscribe((data: SelectEventData) => {
            this.isShowCategoryDetail = false;
            this.isDisabledDeleteBtn = true;
        });
    }

    public addBtnClick() {
        this.categoryTable.dtApi.rows()["deselect"]();
        this.isShowCategoryDetail = true;

        setTimeout(() => { //wait for category component render first
            this.actionEvent.emit({
                event: CategoryComponentEvent.AddBtnClick
            });
        }, 0);
    }

    public deleteBtnClick() {
        let selectedCategory: CategoryTableModel = this.categoryTable.getSelectedData()[0];
        if (selectedCategory) {
            this.categoryService.deleteCategory(selectedCategory).subscribe((result) => {
                this.isShowCategoryDetail = false;
                this.isDisabledDeleteBtn = true;
                this.categoryTable.dtApi.ajax.reload();
            });
        }
    }

    private onTableRowClick(data: SelectEventData) {
        this.isShowCategoryDetail = true;
        this.isDisabledDeleteBtn = false;

        setTimeout(() => { //wait for category component render first
            this.actionEvent.emit({
                event: CategoryComponentEvent.CategoryTableRowClick,
                data: data
            });
        }, 0);
    }

    private setupCategoryTable() {
        this.categoryTableSetting = {
            tableId: "category-table",
            ajax: {
                url: this.httpHelper.resolveUrl({ url: "api/Category/GetCategories" })
            },
            select: { style: "single" },
            columns: [
                {
                    title: "Id", data: "Id",
                    visible: false,
                    defaultSort: {
                        sortOrder: 0,
                        sortType: ColumnSortType.DESC 
                    }
                },
                {
                    title: "Name", data: "Name"
                },
                {
                    title: "Description", data: "Description"
                }
            ]
        };
    }

    private subscribeActionEvent() {
        this.actionEvent.subscribe((eventModel: ActionEventModel) => {
            switch (true) {
                case (eventModel.event == CategoryComponentEvent.CancelBtnClick): {
                    this.isShowCategoryDetail = false;
                    break;
                }
                case (eventModel.event == CategoryComponentEvent.SaveCompleted): {
                    this.isShowCategoryDetail = false;
                    this.categoryTable.dtApi.ajax.reload();
                    break;
                }
                case (eventModel.event == CategoryComponentEvent.EditCompleted): {
                    this.categoryTable.dtApi.ajax.reload();
                    break;
                }
            }
        });
    }
}