﻿import { Component, ViewChild, AfterViewInit, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { ColumnSortType, CategoryComponentEvent } from "../../../common/enums/common.enum";
import { Input, EventEmitter } from "@angular/core";
import { HttpHelpers, CustomValidationService, CategoryService } from "../../../services/all.services";
import { SelectEventData } from "../../../models/all.models";

@Component({
    selector: "admin-category-detail",
    templateUrl: "admin.category-detail.component.html",
    styleUrls: ["admin.category-detail.component.css"]
})
export class AdminCategoryDetailComponent implements OnInit {
    @Input() actionEvent: EventEmitter<ActionEventModel>;

    public legendTagTitle: string = "";
    public isShowCancelBtn: boolean = false;
    public categoryDetailForm: FormGroup;

    private selectedRow: CategoryTableModel;

    constructor(
        private httpHelper: HttpHelpers,
        private formBuilder: FormBuilder,
        private categoryService: CategoryService) {
       
    }

    public ngOnInit() {
        this.subscribeActionEvent();
        this.buildForm();
    }

    public saveBtnClick() {
        this.nameCtrl.markAsTouched();
        this.descriptionCtrl.markAsTouched();

        if (this.categoryDetailForm.valid) {
            let category = this.categoryDetailForm.getRawValue();
            if (this.selectedRow) {
                category.Id = this.selectedRow.Id;
                this.categoryService.editCategory(category).subscribe(result => {
                    this.actionEvent.emit({
                        event: CategoryComponentEvent.EditCompleted
                    });
                });
            } else {
                this.categoryService.saveCategory(category).subscribe(result => {
                    this.actionEvent.emit({
                        event: CategoryComponentEvent.SaveCompleted
                    });
                });
            }
        }
    }

    public resetBtnClick() {
        if (this.selectedRow) {
            this.categoryDetailForm.reset(this.selectedRow);
        } else {
            this.categoryDetailForm.reset({
                Name: "",
                Description: ""
            });
        }
    }

    public cancelBtnClick() {
        this.actionEvent.emit({
            event: CategoryComponentEvent.CancelBtnClick
        });
    }

    private subscribeActionEvent() {
        this.actionEvent.subscribe((eventModel: ActionEventModel) => {
            switch (true) {
                case (eventModel.event == CategoryComponentEvent.AddBtnClick): {
                    this.addBtnClick();
                    break;
                }
                case (eventModel.event == CategoryComponentEvent.CategoryTableRowClick): {
                    this.onTableRowClick(eventModel.data);
                    break;
                }
            }
        });
    }

    private buildForm() {
        this.categoryDetailForm = this.formBuilder.group({
            Name: ["", CustomValidationService.required("Name")],
            Description: ["", CustomValidationService.required("Description")]
        });
    }

    public get nameCtrl() {
        return this.categoryDetailForm.get("Name");
    }

    public get descriptionCtrl() {
        return this.categoryDetailForm.get("Description");
    }

    private onTableRowClick(eventData: SelectEventData) {
        this.legendTagTitle = "Edit Category";
        this.isShowCancelBtn = false;
        this.selectedRow = eventData.data;
        
        this.categoryDetailForm.reset({
            Name: this.selectedRow.Name,
            Description: this.selectedRow.Description
        });
    }

    private addBtnClick() {
        this.legendTagTitle = "Add New Category";
        this.isShowCancelBtn = true;
        this.selectedRow = null;
        this.categoryDetailForm.reset();
    }
}