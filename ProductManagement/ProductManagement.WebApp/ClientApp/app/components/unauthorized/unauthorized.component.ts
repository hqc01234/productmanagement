import { Component } from "@angular/core";
import { AuthenticationService } from "../../services/authentication.service";

@Component({
    selector: "unauthorized",
    templateUrl: "unauthorized.component.html"
})
export class UnauthorizedComponent {

    constructor(private authService: AuthenticationService) {

    }

    public signOut() {
        this.authService.signOutRedirect();
    }
}
