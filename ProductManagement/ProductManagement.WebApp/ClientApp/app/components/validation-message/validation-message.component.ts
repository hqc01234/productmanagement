import { Component, Input } from "@angular/core";
import { FormControl } from "@angular/forms";
import { CustomValidationService } from "../../services/all.services";

@Component({
    selector: "validation-message",
    template: `
        <div *ngIf="errorMessage !== null" style="color: red;">{{errorMessage}}</div>
    `
})
export class ValidationMessageComponent {
    @Input() control: FormControl;

    get errorMessage() {
        for (let errorName in this.control.errors) {
            if (this.control.hasError(errorName) && this.control.touched) {
                return CustomValidationService.getValidatorErrorMessage(errorName, this.control.getError(errorName));
            }
        }
        return null;
    }
}
