import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";
import { LoadingModule, ANIMATION_TYPES } from "ngx-loading";

import {
    AppComponent,
    AdminCategoryComponent,
    AdminProductComponent,
    AdminNavigationComponent,
    DatatableComponent,
    AuthenticationCallBackComponent,
    InternalServerErrorComponent,
    UnauthorizedComponent,
    ValidationMessageComponent,
    AdminCategoryDetailComponent
} from "./components/all.components";

import {
    AuthenticationGuard,
    AuthenticationService,
    UtilitiesService,
    HttpHelpers,
    CategoryService
} from "./services/all.services";

@NgModule({
    declarations: [
        AppComponent,
        AdminCategoryComponent,
        AdminProductComponent,
        AdminNavigationComponent,
        DatatableComponent,
        AuthenticationCallBackComponent,
        InternalServerErrorComponent,
        UnauthorizedComponent,
        ValidationMessageComponent,
        AdminCategoryDetailComponent
    ],
    providers: [
        AuthenticationService, AuthenticationGuard, UtilitiesService, HttpHelpers, CategoryService
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule, 
        ReactiveFormsModule,
        LoadingModule.forRoot({
            animationType: ANIMATION_TYPES.threeBounce,
            backdropBackgroundColour: "rgba(0, 0, 0, 0.1)",
            backdropBorderRadius: "0px",
            primaryColour: "#f0ad4e",
            secondaryColour: "#f0ad4e",
            tertiaryColour: "#f0ad4e"
        }),
        RouterModule.forRoot([
            { path: "", redirectTo: "category", pathMatch: "full" },
            { path: "authentication-callback", component: AuthenticationCallBackComponent },
            { path: "internal-server-error", component: InternalServerErrorComponent },
            { path: "unauthorized", component: UnauthorizedComponent },
            { path: "category", component: AdminCategoryComponent, canActivate: [AuthenticationGuard] },
            { path: "product", component: AdminProductComponent, canActivate: [AuthenticationGuard] },
            { path: "**", redirectTo: "category" }
        ], { useHash: true })
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModuleShared {
}
