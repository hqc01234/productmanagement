﻿declare type ConstructorData<T> = {
    [P in keyof T]: T[P];
}

declare type HttpParameters = {
    url?: string;
    data?: {[key: string]: any;}
}
