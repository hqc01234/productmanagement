﻿export enum FontAwesomeIcon { 
    fa_address_book = "fa-address-book",
    fa_circle_o = "fa-circle-o",
    fa_image = "fa-image",
    fa_angle_left = "fa-angle-left"
}

export enum ColumnSortType {
    ASC = "asc",
    DESC = "desc",
    NONE = ""
}

export enum UserRole {
    Admin = "Admin",
    User = "User"
}

export enum CategoryComponentEvent {
    AddBtnClick = 1,
    CategoryTableRowClick = 2,
    SaveBtnClick = 3,
    CancelBtnClick = 4,
    SaveCompleted = 5,
    EditCompleted = 6,
}