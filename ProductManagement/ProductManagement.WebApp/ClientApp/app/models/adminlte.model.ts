﻿interface AdminLTE_JQueryObject extends JQueryStatic {
    AdminLTE: AdminLTE;
}

interface AdminLTE {
    options: AdminLTE_Option;
    layout: {
        fix: () => any
    };
}

interface AdminLTE_Option {
    animationSpeed: number;
}