﻿import { FontAwesomeIcon } from "../common/enums/common.enum";

export class NavigationItem {
    public name: string;   
    public icon: FontAwesomeIcon;
    public routeLink?: string;
    public openByDefault?: boolean;
    public childItems?: Array<NavigationItem>;

    constructor(init: ConstructorData<NavigationItem>) {
        this.routeLink = "#";
        this.openByDefault = false;
        this.childItems = [];

        Object.assign(this, init);
    }
}