﻿import { ColumnSortType } from "../common/enums/common.enum";

export interface DataTablesSetting<T> {
    tableId: string;
    columns: Array<DataTablesColumn<T>>;
    ajax?: DataTablesAjax; 
    /** Data source from object */
    data?: Object; 
    dom?: string;
    serverSide?: boolean;
    processing?: boolean;
    language?: DataTables.LanguageSettings;
    select?: {
        enable?: boolean,
        style?: "os" | "single" | "multi",
        info?: boolean,
        checkBox?: boolean
    };
}

export interface DataTablesAjax {
    url: string;
    type?: "POST" | "GET",
    data?: Object | DataTables.FunctionAjaxData;
}

export interface DataTablesColumn<T> {
    /** Column title */
    title: string;
    /** Property name of data model */
    data: keyof T;
    sortable?: boolean;
    defaultSort?: {
        /** For multiple-column sort, lower = sort first */
        sortOrder: number;
        sortType: ColumnSortType;
    };
    visible?: boolean;
    width?: string;
    className?: string;
    render?: (data: any, type: any, row: T, meta: DataTables.CellMetaSettings) => any;
}

export interface SelectEventData {
    rowIndex?: number;
    data?: any;
}