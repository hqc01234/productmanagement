﻿interface CategoryTableModel {
    Id: number;
    Name: string;
    Description: string;
}