﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProductManagement.Core.Datatables;
using ProductManagement.Core.DTOs;
using ProductManagement.Core.ServiceInterfaces;
using ProductManagement.Services.DataTables;

namespace ProductManagement.WebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this._categoryService = categoryService;
        }

        [HttpGet("[action]")]
        public JsonResult GetCategories(DataTableParameterModel param)
        {
            var result = this._categoryService.GetCategories().ConvertToDataSource(param);
            return this.Json(result);
        }

        [HttpPost("[action]")]
        public JsonResult SaveCategory(CategoryDto category)
        {
            this._categoryService.SaveCategory(category);
            return this.Json(true);
        }

        [HttpPost("[action]")]
        public JsonResult DeleteCategory(CategoryDto category)
        {
            this._categoryService.DeleteCategory(category);
            return this.Json(true);
        }

        [HttpPost("[action]")]
        public JsonResult EditCategory(CategoryDto category)
        {
            this._categoryService.UpdateCategory(category);
            return this.Json(true);
        }
    }
}
