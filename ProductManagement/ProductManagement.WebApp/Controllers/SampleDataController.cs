using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using ProductManagement.Core.Datatables;
using ProductManagement.Core.DTOs;
using ProductManagement.Services.DataTables;

namespace TST.WebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        [HttpGet("[action]")]
        public DataTableDataSource<CategoryDto> GetCategories(DataTableParameterModel param)
        {
            var data = new List<CategoryDto>()
            {
                new CategoryDto()
                {
                    Id = 1,
                    Description = "Test data 1"
                },
                new CategoryDto()
                {
                    Id = 2,
                    Description = "Test data 2"
                }
            }.AsQueryable();

            var result = data.ConvertToDataSource(param);
            return result;
        }
    }
}
