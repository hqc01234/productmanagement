using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Core.ServiceInterfaces;

namespace ProductManagement.WebApi.Controllers
{
    public class HomeController : Controller
    {
        private ICategoryService MySerive { get; set; }

        public HomeController(ICategoryService mySerive)
        {
            this.MySerive = mySerive;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
