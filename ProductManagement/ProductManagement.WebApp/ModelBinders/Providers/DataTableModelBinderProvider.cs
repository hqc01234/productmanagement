﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System;
using ProductManagement.Core.Datatables;

namespace ProductManagement.WebApi.ModelBinders.Providers
{
    public class DataTableModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(DataTableParameterModel))
            {
                return new BinderTypeModelBinder(typeof(DataTableModelBinder));
            }

            return null;
        }
    }
}
