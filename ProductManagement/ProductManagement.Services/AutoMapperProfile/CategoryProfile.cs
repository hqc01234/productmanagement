﻿using AutoMapper;
using ProductManagement.Core.DTOs;
using ProductManagement.Data.Entities;

namespace ProductManagement.Services.AutoMapperProfile
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
        }
    }
}
