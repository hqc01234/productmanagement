﻿using AutoMapper;
using ProductManagement.Core.DTOs;
using ProductManagement.Data.Entities;

namespace ProductManagement.Services.AutoMapperProfile
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductDto>().ReverseMap();
        }
    }
}
