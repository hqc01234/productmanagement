﻿using ProductManagement.Core.Datatables;
using System.Linq.Dynamic.Core;
using System.Linq;

namespace ProductManagement.Services.DataTables
{
    public static class DataTableUtilities
    {
        public static DataTableDataSource<T> ConvertToDataSource<T>(this IQueryable<T> data, DataTableParameterModel param)
        {
            var sortedColumns = param.Order;
            var orderByString = string.Empty;

            var total = data.Count();

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != string.Empty ? "," : "";
                orderByString += (param.Columns.ElementAt(column.Column).Data) + (column.Dir == "asc" ? " asc" : " desc");
            }

            if(orderByString != string.Empty)
            {
                data = data.OrderBy(orderByString);
            }

            var result = data.Skip(param.Start).Take(param.Length).ToList();

            return new DataTableDataSource<T>
            {
                draw = param.Draw,
                recordsTotal = total,
                recordsFiltered = total,
                data = result
            };
        }
    }
}
