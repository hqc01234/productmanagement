﻿using ProductManagement.Data.Entities;
using ProductManagement.Core.ServiceInterfaces;
using ProductManagement.Data.Repository.Interface;
using ProductManagement.Core.DTOs;
using AutoMapper;
using System.Linq;
using AutoMapper.QueryableExtensions;

namespace ProductManagement.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Category> _categoryRepository;

        public CategoryService(IRepository<Category> categoryRepository, IMapper mapper)
        {
            this._categoryRepository = categoryRepository;
            this._mapper = mapper;
        }

        public void SaveCategory(CategoryDto categoryDto)
        {
            var categoryEntity = this._mapper.Map<Category>(categoryDto);
            this._categoryRepository.Insert(categoryEntity);
            this._categoryRepository.SaveChanges();
        }

        public void UpdateCategory(CategoryDto categoryDto)
        {
            var categoryEntity = this._mapper.Map<Category>(categoryDto);
            this._categoryRepository.Update(categoryEntity);
            this._categoryRepository.SaveChanges();
        }

        public void DeleteCategory(CategoryDto categoryDto)
        {
            var categoryEntity = this._mapper.Map<Category>(categoryDto);
            this._categoryRepository.Delete(categoryEntity);
            this._categoryRepository.SaveChanges();
        }

        public IQueryable<CategoryDto> GetCategories()
        {
            return this._categoryRepository.Query().ProjectTo<CategoryDto>();
        }
    }
}
