﻿using Autofac;
using Microsoft.Extensions.Configuration;
using ProductManagement.Core.ServiceInterfaces;
using ProductManagement.Data;
using System.Reflection;

namespace ProductManagement.Services
{
    public class ServicesModule : Autofac.Module
    {
        private IConfiguration _configuration { get; set; }

        public ServicesModule(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                   .AssignableTo<IService>()
                   .AsImplementedInterfaces();

            builder.RegisterModule(new DataModule(this._configuration));
        }
    }
}
