﻿using ProductManagement.Core.DTOs;
using System.Linq;

namespace ProductManagement.Core.ServiceInterfaces
{
    public interface ICategoryService : IService
    {
        void SaveCategory(CategoryDto categoryDto);
        void UpdateCategory(CategoryDto categoryDto);
        void DeleteCategory(CategoryDto categoryDto);
        IQueryable<CategoryDto> GetCategories();
    }
}
