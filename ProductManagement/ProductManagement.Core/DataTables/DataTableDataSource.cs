﻿using System.Collections.Generic;

namespace ProductManagement.Core.Datatables
{
    public class DataTableDataSource<TEntity>
    {
        public IEnumerable<TEntity> data { get; set; }
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
}
