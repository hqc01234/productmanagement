﻿namespace ProductManagement.Core.DTOs
{
    public class BaseDto<TId>
    {
        public TId Id { get; set; }
    }
}
