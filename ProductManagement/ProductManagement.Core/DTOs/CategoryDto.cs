﻿using System.Collections.Generic;

namespace ProductManagement.Core.DTOs
{
    public class CategoryDto : BaseDto<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual IEnumerable<ProductDto> Products { get; set; }
    }
}
