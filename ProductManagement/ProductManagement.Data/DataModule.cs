﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProductManagement.Data.DBContext;
using ProductManagement.Data.Repository.Implement;
using ProductManagement.Data.Repository.Interface;

namespace ProductManagement.Data
{
    public class DataModule: Module
    {
        private IConfiguration _configuration { get; set; }

        public DataModule(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<,,>))
                .As(typeof(IRepository<,>));

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>));
        }
    }
}
