﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ProductManagement.Data.Extensions
{    
    public static class ModelBuilderExtensions
    {
        private static IEnumerable<Type> GetMappingTypes(this Assembly assembly, Type mappingInterface)
        {
            return assembly.GetTypes()
                           .Where(x => !x.GetTypeInfo().IsAbstract &&
                                        x.GetInterfaces().Any(y => y.GetTypeInfo().IsGenericType && y.GetGenericTypeDefinition() == mappingInterface));
        }

        public static void AddEntityConfigurationsFromAssembly(this ModelBuilder modelBuilder, Assembly assembly)
        {
            var mappingTypes = assembly.GetMappingTypes(typeof(IEntityMappingConfiguration<>))
                                       .Select(Activator.CreateInstance)
                                       .Cast<IEntityMappingConfiguration>();

            foreach (var config in mappingTypes)
            {
                config.Map(modelBuilder);
            }
        }
    }
}
