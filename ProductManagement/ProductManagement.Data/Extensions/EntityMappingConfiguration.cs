﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProductManagement.Data.Extensions
{
    public interface IEntityMappingConfiguration
    {
        void Map(ModelBuilder modelBuilder);
    }

    public interface IEntityMappingConfiguration<T> : IEntityMappingConfiguration where T : class
    {
        void Map(EntityTypeBuilder<T> entityTypeBuilder);
    }

    public abstract class EntityMappingConfiguration<T> : IEntityMappingConfiguration<T> where T : class
    {
        public abstract void Map(EntityTypeBuilder<T> entityTypeBuilder);

        public void Map(ModelBuilder modelBuilder)
        {
            Map(modelBuilder.Entity<T>());
        }
    }
}
