﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductManagement.Data.Entities;
using ProductManagement.Data.Extensions;

namespace ProductManagement.Data.Maps
{
    public class CategoryMap : EntityMappingConfiguration<Category>
    {
        public override void Map(EntityTypeBuilder<Category> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Category");
            entityTypeBuilder.HasMany(x => x.Products);
        }
    }
}
