﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductManagement.Data.Entities;
using ProductManagement.Data.Extensions;

namespace ProductManagement.Data.Maps
{
    public class ProductMap : EntityMappingConfiguration<Product>
    {
        public override void Map(EntityTypeBuilder<Product> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Product");
            entityTypeBuilder.HasOne(x => x.Category)
                             .WithMany(x => x.Products)
                             .HasForeignKey(x => x.CategoryId);
        }
    }
}
