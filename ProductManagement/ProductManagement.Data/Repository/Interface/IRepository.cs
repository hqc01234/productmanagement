﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Linq;
using ProductManagement.Data.Entities;

namespace ProductManagement.Data.Repository.Interface
{
    public interface IRepository<TEntity, TId> 
        where TEntity : BaseEntity<TId>
    {
        IQueryable<TEntity> Query();

        TEntity GetById(TId id);

        IEnumerable<TEntity> GetAll();

        EntityEntry Insert(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        int SaveChanges();
    }

    public interface IRepository<TEntity> : IRepository<TEntity, int>
        where TEntity : BaseEntity<int>
    {

    }
}
