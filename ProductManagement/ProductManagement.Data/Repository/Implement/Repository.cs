﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Linq;
using ProductManagement.Data.Entities;
using ProductManagement.Data.DBContext;
using ProductManagement.Data.Repository.Interface;

namespace ProductManagement.Data.Repository.Implement
{
    public class Repository<TContext, TEntity, TId> : IRepository<TEntity, TId> 
        where TEntity : BaseEntity<TId>
        where TContext : DbContext
    {
        private readonly TContext context;

        public Repository(TContext context)
        {
            this.context = context;
        }

        private DbSet<TEntity> GetEntities()
        {
            return this.context.Set<TEntity>();
        }

        public IQueryable<TEntity> Query()
        {
            return this.GetEntities().AsQueryable();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this.GetEntities().ToList();
        }

        public TEntity GetById(TId id)
        {
            return this.Query().SingleOrDefault(x => x.Id.Equals(id));
        }

        public EntityEntry Insert(TEntity entity)
        {
            return this.GetEntities().Add(entity);
        }

        public void Update(TEntity entity)
        {
            var original = GetById(entity.Id);
            if (original != null)
            {
                this.context.Entry(original).CurrentValues.SetValues(entity);
            }
        }

        public void Delete(TEntity entity)
        {
            var original = GetById(entity.Id);
            if (original != null)
            {
                this.GetEntities().Remove(original);
            }
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }
    }

    //default repository with AppDbContext and int Id
    public class Repository<TEntity> : Repository<AppDbContext, TEntity, int>, IRepository<TEntity>
        where TEntity : BaseEntity<int>
    {
        public Repository(AppDbContext context) : base(context)
        {
        }
    }
}
