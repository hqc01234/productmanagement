﻿using IdentityServer.Models;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using QuickstartIdentityServer;
using System.Linq;

namespace IdentityServer.Data
{
    public static class InitializeDatabase
    {
        public static async void InitDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {              
                serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                var appDbcontext = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                appDbcontext.Database.Migrate();
                if (!appDbcontext.Roles.Any() && !appDbcontext.Users.Any() && !appDbcontext.UserRoles.Any())
                {
                    appDbcontext.Roles.Add(new IdentityRole() { Id = "1", Name = "Admin", NormalizedName = "Admin" });
                    appDbcontext.Roles.Add(new IdentityRole() { Id = "2", Name = "User", NormalizedName = "User" });

                    var _userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    var user = new ApplicationUser {
                        UserName = "admin@gmail.com",
                        Email = "admin@gmail.com"
                    };
                    var result = await _userManager.CreateAsync(user, "!Abc123");
                    await _userManager.AddToRoleAsync(user, "Admin");

                    appDbcontext.SaveChanges();
                }

                var configDbcontext = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                configDbcontext.Database.Migrate();
                if (!configDbcontext.Clients.Any())
                {
                    foreach (var client in Config.GetClients())
                    {
                        configDbcontext.Clients.Add(client.ToEntity());
                    }
                    configDbcontext.SaveChanges();
                }

                if (!configDbcontext.IdentityResources.Any())
                {
                    foreach (var resource in Config.GetIdentityResources())
                    {
                        configDbcontext.IdentityResources.Add(resource.ToEntity());
                    }
                    configDbcontext.SaveChanges();
                }

                if (!configDbcontext.ApiResources.Any())
                {
                    foreach (var resource in Config.GetApiResources())
                    {
                        configDbcontext.ApiResources.Add(resource.ToEntity());
                    }
                    configDbcontext.SaveChanges();
                }
            }
        }
    }
}
