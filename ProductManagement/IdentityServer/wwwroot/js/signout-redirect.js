﻿window.addEventListener("load", function () {
    setTimeout(() => {
        var a = document.querySelector("a.PostLogoutRedirectUri");
        if (a) {
            window.location = a.href;
        }
    }, 1000);
});
